package com.js.resumeapi.controller;

import com.js.resumeapi.model.ResumeRequest;
import com.js.resumeapi.service.ResumeService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/resume")
public class ResumeController {
    private final ResumeService resumeService;

    @PostMapping("/registration")
    public String setResume(@RequestBody ResumeRequest request){
        resumeService.setResume(request);
        return "입사지원이 완료되었습니다.";
    }
}

