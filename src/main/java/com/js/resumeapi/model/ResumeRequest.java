package com.js.resumeapi.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ResumeRequest {
    private String name;
    private Byte age;
    private String address;
    private String education;
    private String award;
    private String workExperience;
}
